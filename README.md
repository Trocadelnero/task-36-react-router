# Task 36: React Router

## Instructions/Description
This is the 3rd Task involving React.\
Continuation from [T35](https://gitlab.com/Trocadelnero/task-35-react-component-events) \
-[x] Install the React Router Dom \
Define be 3 main routes: Login, Register and Dashboard. \
-[x] The first screen that must be shown is the Login. \
-[x] The user must be able to navigate from the Login to the Register page. \
-[XXX] On successful Registration, the user must be navigated to the Dashboard programatically. \
-[x] Create a 404 Not Found page that is displayed when the user types an unknown path in the address bar of the browser. \


**App.css**
- green/yellow theme with focus on borderstyles around elements.
- Still learning how to implement correctly/efficiently.
- Personal theme *under development*, used in various projects.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts
In the project directory, you can run:
### `npm install`
Install dependencies e.g if you cloned this project.

### `npm start`
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`
Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`
See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More
You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
To learn React, check out the [React documentation](https://reactjs.org/).

### Making a Progressive Web App
This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration
This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration
