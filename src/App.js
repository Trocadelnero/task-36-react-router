import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Redirect,
} from "react-router-dom";
import "./App.css";
import Login from "./components/containers/Login";
import Home from "./components/containers/Home";
import Dashboard from "./components/containers/Dashboard";
import Register from "./components/containers/Register";
import NotFound from "./components/containers/NotFound";

function App() {
  return (
    <Router>
      <div className="App">
        My first "Reaction"
        <nav>
          <ul>
            <NavLink to="/home">Home</NavLink>
            <NavLink to="/dashboard">Dashboard</NavLink>
            <NavLink to="/login">Login</NavLink>
            <NavLink to="/register">Register</NavLink>
          </ul>
        </nav>
        <Switch>
          <Route exact path="/">
            <Redirect to="/login"></Redirect>
          </Route>
          <Route path="/home" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
