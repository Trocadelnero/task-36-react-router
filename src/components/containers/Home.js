import React from "react";
import { useHistory } from "react-router-dom";

const Home = (props) => {
  
  const history = useHistory();

  const OnGoHomeClicked = () => {
    history.replace("/dashboard");
  };
  return (
    <React.Fragment>
      <h1>Welcome Home</h1>
      <button onClick={OnGoHomeClicked}>Go to Dashboard</button>
    </React.Fragment>
  );
};

export default Home;
