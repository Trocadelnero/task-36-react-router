import React, { Fragment } from "react";
import DashboardMessage from "../presentational/DashboardMessage";

class Dashboard extends React.Component {
  render() {
    return (
      <Fragment>
        <DashboardMessage />
      </Fragment>
    );
  }
}

export default Dashboard;
