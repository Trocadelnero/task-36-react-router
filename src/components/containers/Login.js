import React from "react";
import LoginForm from "../forms/LoginForm";
import { Link } from "react-router-dom";

class Login extends React.Component {
  
  handleLoginComplete = (result) => {
    if (result === true) {
      this.props.history.push("/dashboard");
    }
  };

  render() {
    return (
      <React.Fragment>
        <h1> Login to survey puppy</h1>

        <LoginForm complete={this.handleLoginComplete} />

        <Link to="/register">Register Here!</Link>
      </React.Fragment>
    );
  }
}

export default Login;
