import React from "react";
import { Link } from "react-router-dom";

class NotFound extends React.Component {
  //Init state
  // state = {};

  // componentDidMount() {}
  render() {
    return (
      <React.Fragment>
        <br />
        <img
          src="https://image.freepik.com/free-vector/404-error-with-glitch-effect_225004-656.jpg"
          width="404"
          alt="Not Found"
        />
        <a href="https://www.freepik.com/free-photos-vectors/technology">
          Technology vector created by dgim-studio - www.freepik.com
        </a>
        <br />
        <br />
        <Link to="/">Go back Home</Link>
      </React.Fragment>
    );
  }
}

export default NotFound;
