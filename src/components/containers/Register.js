import React, { Fragment } from "react";
import RegisterForm from "../forms/RegisterForm";

import { Link } from "react-router-dom";

class Register extends React.Component {
  handleRegisterComplete = (result) => {
    if (result === true) {
      this.props.history.push("/dashboard");
    }
  };

  render() {
    return (
      <Fragment>
        <h1>Register to Survey Puppy</h1>
        <RegisterForm complete={this.handleRegisterComplete} />

        <Link to="/login">Already Registered?</Link>
      </Fragment>
    );
  }
}

export default Register;
